﻿namespace ItaiTzur.utilities.lazy_button
{
    using ItaiTzur.utilities.lazy_button.Interfaces;
    using System.IO;
    using System.Threading.Tasks;

    internal sealed class FileService : IFileService
    {
        /// <summary>
        /// Asynchronously opens a text file, reads all the text in the file, and then closes the file.
        /// </summary>
        /// <param name="path">The file to open for reading.</param>
        /// <returns>
        /// A task that represents the asynchronous read operation, which wraps the string containing all text in the
        /// file.
        /// </returns>
        public async Task<string> ReadAllTextAsync(string path)
        {
            return await File.ReadAllTextAsync(path);
        }
    }
}
