﻿namespace ItaiTzur.utilities.lazy_button
{
    using ItaiTzur.utilities.lazy_button.Interfaces;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Interactions;
    using System;

    internal sealed class BrowserOperator : IBrowserOperator
    {
        private readonly Lazy<ChromeDriver> _chromeDriver = new Lazy<ChromeDriver>(() => NewMaximizedChromeDriver());

        private static ChromeDriver NewMaximizedChromeDriver()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("start-maximized");
            // If the following line throws a DriverServiceNotFoundException then the Selenium.WebDriver.ChromeDriver package is probably not installed.
            // If it throws an InvalidOperationException then its Message contains relevant information. For example, if the Message looks like this:
            //     session not created: This version of ChromeDriver only supports Chrome version {REFERENCED BROWSER VERSION}
            //     Current browser version is {INSTALLED BROWSER VERSION} with binary path C:\Program Files (x86)\Google\Chrome\Application\chrome.exe (SessionNotCreated)
            // then the PackageReference to Selenium.WebDriver.ChromeDriver probably has to be updated to {INSTALLED BROWSER VERSION} in the csproj file.
            // One way to do that in VS is to open "NuGet Package Ma&nager" under the &Tools menu and select "Manage &NuGet Packages for Solution...", then select Selenium.WebDriver.ChromeDriver in the Installed tab, tick the current project, select {INSTALLED BROWSER VERSION} from the Version list-box, click on Install and then on OK.
            var chromeDriver = new ChromeDriver(chromeOptions);
            return chromeDriver;
        }

        private ChromeDriver ChromeDriverInstance()
        {
            return _chromeDriver.Value;
        }

        public void NavigateTo(string url)
        {
            ChromeDriverInstance().Navigate().GoToUrl(url);
        }

        public void Quit()
        {
            ChromeDriverInstance().Quit();
        }

        public void SendKeys(string keysToSend)
        {
            Perform(actions => actions.SendKeys(keysToSend));
        }

        public void ShiftDown()
        {
            Perform(actions => actions.KeyDown(Keys.Shift));
        }

        public void ShiftUp()
        {
            Perform(actions => actions.KeyUp(Keys.Shift));
        }

        private void Perform(Func<Actions, Actions> action)
        {
            action(new Actions(ChromeDriverInstance())).Build().Perform();
        }
    }
}
