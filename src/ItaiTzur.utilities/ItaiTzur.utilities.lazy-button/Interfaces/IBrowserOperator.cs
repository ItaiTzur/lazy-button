﻿namespace ItaiTzur.utilities.lazy_button.Interfaces
{
    internal interface IBrowserOperator
    {
        void NavigateTo(string url);
        void Quit();
        void SendKeys(string keysToSend);
        void ShiftDown();
        void ShiftUp();
    }
}
