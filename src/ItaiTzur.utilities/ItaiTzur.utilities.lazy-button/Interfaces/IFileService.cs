﻿namespace ItaiTzur.utilities.lazy_button.Interfaces
{
    using System.Threading.Tasks;

    internal interface IFileService
    {
        Task<string> ReadAllTextAsync(string path);
    }
}
