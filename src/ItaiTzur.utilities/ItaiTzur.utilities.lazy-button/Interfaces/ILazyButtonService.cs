﻿namespace ItaiTzur.utilities.lazy_button.Interfaces
{
    using System.Threading.Tasks;

    internal interface ILazyButtonService
    {
        Task<string> StartAsync(string inputFilePath);
    }
}
