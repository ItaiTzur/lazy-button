﻿namespace ItaiTzur.utilities.lazy_button
{
    using ItaiTzur.utilities.lazy_button.Interfaces;
    using OpenQA.Selenium;
    using System;
    using System.IO;
    using System.Threading.Tasks;

    internal sealed class LazyButtonService : ILazyButtonService
    {
        private readonly IBrowserOperator _browserOperator;
        private readonly IFileService _fileService;

        public LazyButtonService(IBrowserOperator browserOperator, IFileService fileService)
        {
            _browserOperator = browserOperator;
            _fileService = fileService;
        }

        public async Task<string> StartAsync(string inputFilePath)
        {
            string textToProcess;
            try
            {
                textToProcess = await _fileService.ReadAllTextAsync(inputFilePath);
            }
            catch (FileNotFoundException)
            {
                return $"File \"{inputFilePath}\" not found.";
            }

            var newLine = Environment.NewLine;
            var firstNewLineIndex = textToProcess.IndexOf(newLine);
            var url = textToProcess;
            if (firstNewLineIndex >= 0)
            {
                url = textToProcess[..firstNewLineIndex];
            }

            try
            {
                _browserOperator.NavigateTo(url);
            }
            catch (WebDriverException webDriverException) when (webDriverException.Message.StartsWith("unknown error: net::ERR_INTERNET_DISCONNECTED"))
            {
                _browserOperator.Quit();
                return "Web-driver error. This might indicate that the Internet is disconnected.";
            }
            catch (WebDriverArgumentException)
            {
                _browserOperator.Quit();
                return
                    $"Web-driver argument error. This might indicate that the requested web-page (\"{url}\") is invalid.";
            }
            catch (InvalidOperationException invalidOperationException) when (invalidOperationException.Message.StartsWith("session not created: This version of ChromeDriver only supports Chrome version "))
            {
                return
                    @"This application tried to open an outdated Chrome-driver version.
Please open the source code's solution in Visual Studio, go to
""Tools"" --> ""NuGet Package Manager"" --> ""Manage NuGet Packages for Solution..."",
open the Updates tab, tick the checkbox next to ""Selenium.WebDriver.ChromeDriver""
and click on Update and then on OK to update the Chrome-driver version.
Then rebuild the solution (after closing this window) and rerun the application.";
            }

            if (firstNewLineIndex < 0)
            {
                return null;
            }

            textToProcess = textToProcess[(firstNewLineIndex + newLine.Length)..];
            if (string.IsNullOrEmpty(textToProcess))
            {
                return null;
            }

            firstNewLineIndex = textToProcess.IndexOf(newLine);
            if (firstNewLineIndex < 0)
            {
                return null;
            }

            var keys = firstNewLineIndex >= 0
                ? textToProcess[(firstNewLineIndex + newLine.Length)..].Split(textToProcess[..firstNewLineIndex])
                : new[] { textToProcess };
            var segments = keys.Length - 1;
            var segment = 0;
            do
            {
                _browserOperator.SendKeys(keys[segment]);
                if (segment == segments)
                {
                    return null;
                }

                if (segment % 2 == 0)
                {
                    _browserOperator.ShiftDown();
                }
                else
                {
                    _browserOperator.ShiftUp();
                }

                ++segment;
            } while (true);
        }
    }
}
