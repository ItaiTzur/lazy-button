﻿namespace ItaiTzur.utilities.lazy_button
{
    using ItaiTzur.utilities.lazy_button.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Threading.Tasks;

    internal sealed class Program
    {
        private async static Task Main(string[] args)
        {
            if (args.Length < 1)
            {
                var usage = @$"Usage: {Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName)} <input file>
The first row of the input file should contain a URL to open.
Its second row should contain the string used to represent Shift-key toggle, or nothing if such a toggle is not needed.
All subsequent rows should contain the keys to send to that URL, which may include the Shift-key toggle string defined.";
                Console.WriteLine(usage);
                return;
            }

            using var serviceProvider = SetupDependencyInjection();
            var error = await serviceProvider.GetService<ILazyButtonService>().StartAsync(args[0]);
            if (error != null)
            {
                Console.WriteLine(error);
            }
        }

        private static ServiceProvider SetupDependencyInjection()
        {
            return new ServiceCollection()
                .AddSingleton<IBrowserOperator, BrowserOperator>()
                .AddSingleton<IFileService, FileService>()
                .AddSingleton<ILazyButtonService, LazyButtonService>()
                .BuildServiceProvider();
        }
    }
}
